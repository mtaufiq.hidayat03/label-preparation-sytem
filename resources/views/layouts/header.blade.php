<!-- start: header -->
<header class="header">

    <div class="header-left">
        <a class="logo">
            <img src="{{ URL::asset('img/lg_logo.png')}}" width="120" height="60" alt="LG Electronics Indonesia"/>
        </a>
        <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html"
             data-fire-event="sidebar-left-opened">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>
    <!-- start: search & user box -->
    <div class="header-right">

        <span class="separator"></span>

        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="{{ URL::asset('img/sample_user.png')}}" alt="{{Session::get('user_id')}}" class="rounded-circle"
                         data-lock-picture="img/sample_user.png"/>
                </figure>
                <div class="profile-info" data-lock-name="{{Session::get('user_id')}}" data-lock-email="{{Session::get('email')}}">
                    <span class="name"><b>{{Session::get('user_id')}}</b></span>
                    <!-- <span class="role">{{Session::get('user_level')}}</span> -->
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled mb-2">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fas fa-user"></i>
                            My Profile</a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="#modalLogout" class="buttonModalLogout"><i class="fas fa-power-off"></i>
                            Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>
<!-- end: header -->
<!-- Modal Animation -->
<div id="modalLogout" class="zoom-anim-dialog modal-block modal-header-color modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Are you sure?</h2>
        </header>
        <div class="card-body">
            <div class="modal-wrapper">
                <div class="modal-icon">
                    <i class="fas fa-question-circle"></i>
                </div>
                <div class="modal-text">
                    <p class="mb-0">Are you sure to logout the system?</p>
                </div>
            </div>
        </div>
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary logout-modal-confirm">Confirm</button>
                    <button class="btn btn-default logout-modal-dismiss">Cancel</button>
                </div>
            </div>
        </footer>
    </section>
</div>

