<!doctype html>
<html class="fixed header-dark">
<head>

    @include('layouts.meta')

    @include('layouts.css')

</head>
<body class="loading-overlay-showing" data-loading-overlay>

@include('layouts.loading')

<!-- start: page -->
<div class="body-sign">
    <div class="center-sign">
        <div class="card-title-sign text-center card-body-modified">
            <img src="{{ URL::asset('img/lg_logo.png')}}" height="90" alt="LG Electronics Indonesia"/>
        </div>
        <div class="panel card-sign">

            <div class="card-body card-body-modified">
                <form action="" id="loginForm" class="loginForm">
                    <div class="mb-3">
                        <label>Username</label>
                        <div class="input-group">
                            <input name="username" type="text" class="form-control form-control-lg username"
                                   placeholder="Input your email/user id/employee no for username" required/>
                            <span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-user"></i>
										</span>
							</span>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label class="float-left">Password</label>
                        <div class="input-group">
                            <input name="password" type="password" class="form-control form-control-lg password"
                                   placeholder="Input your password" required/>
                            <span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input id="RememberMe" name="rememberme" type="checkbox"/>
                                <label for="RememberMe">Remember Me</label>
                            </div>
                        </div> -->
                        <div class="col-sm-12 text-right">
                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-lge" id="sign-in" data-loading-text="Loading..."><i
                                    class="sign-in fas fa-user mr-1"></i>&nbsp;<span class="sign-in-text">Sign In</span></button>
                        </div>
                    </div>
                </form>
                <br/>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-right">
                            <a href="pages-recover-password.html" class="float-right">Lost Password?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <p class="text-center mt-3 mb-3 bottom card-body-modified">
            <b><font style="font-size: 16px">Label Preparation System v1.0 - Beta Version</font></b><br/>
            &copy; Copyright 2020. All Rights Reserved. <br/> LG Electronics
            Indonesia</p>

        <!-- end: page -->

        @include('layouts.js')

        @include('layouts.login-css')

        <!-- add custom javascript here -->
        <script src="{{ URL::asset('js/custom.js')}}"></script>
        <script type="text/javascript">
            postDataAjaxLoginByIdButton("#sign-in", "POST", "/api-v1/login", "#loginForm");
            $(document).on('keypress',function(e) {
                if(e.which == 13) {
                    submitDataAjaxLogin("#sign-in", "POST", "/api-v1/login", "#loginForm");
                }
            });
        </script>
        <!-- end custom javascript here -->
    </div>
</div>
</body>
</html>
