@extends('layouts.app')
@section('css-section')
    @include('layouts.css-datatables');
@stop

@section('content')
    <header class="page-header">
        <h2><a href="{{route('approval-settings')}}"><i class="fas fa-check-circle"></i> Approval Settings</a> <i class="fas fa-angle-right"></i> Add Data </h2>
    </header>
    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <section class="card">
                <header class="card-header">
                    <h2>Upload Label</h2>
                </header>
                <div class="card-body card-body-modified">
                    <form class="form-horizontal form-bordered" action="" id="approvalSettingsForm">
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="default_approval">Default Approval</label>
                            <div class="col-lg-8">
                                <select class="form-control mb-3" id="default_approval" name="default_approval" required="required">
                                    <option value="">Select your option</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="user_id">Work Order (W/O) ID </label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="woid" name="woid" placeholder="Input WOID" required="required">
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="user_id">USER ID </label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="user_id" name="user_id" placeholder="Input user id as approval" required="required">
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 text-left">
                                <a href="{{ URL::previous() }}" class="btn btn-dark" id="back_button">
                                    <i class="back_button_i fas fa-arrow-left"></i> <span class="back-text">Back</span></a>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-lge" id="save_data" data-loading-text="Saving data..."><i
                                        class="save_data_i fas fa-save"></i>&nbsp;<span class="save-text">Save</span></button>
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-warning" id="cancel_button" data-loading-text="Clearing field..."><i
                                        class="cancel_button_i fas fa-trash-alt"></i>&nbsp;<span class="cancel-text">Reset</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    </header>
@stop

@section('js-section')
    <!-- add custom javascript here -->
    <script src="{{ URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript">
        popupConfirmationModal(".buttonModalLogout");
        logoutModalDismiss(".logout-modal-dismiss");
        logoutModalConfirm(".logout-modal-confirm", "GET", "/api-v1/logout");
        nProgressLoading();
        backButton('#back_button');
        cancelButton('#approvalSettingsForm','#cancel_button');
        saveButton("#save_data","POST","/api-v1/approval-settings/save-approval","#approvalSettingsForm");
        defaultApprovalSelect('#default_approval','#woid');
    </script>
    <!-- end custom javascript here -->
@stop

