@extends('layouts.app')
@section('css-section')
    @include('layouts.css-datatables');
@stop

@section('content')
    <header class="page-header">
        <h2><a href="{{route('approval-settings')}}"><i class="fas fa-check-circle"></i> Approval Settings</a> <i class="fas fa-angle-right"></i> Edit Data </h2>
    </header>
    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <section class="card">
                <header class="card-header">
                    <h2>Edit Data</h2>
                </header>
                <div class="card-body card-body-modified">
                    <form class="form-horizontal form-bordered" action="" id="approvalSettingsForm">
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="default_approval">Default Approval</label>
                            <div class="col-lg-8">
                                <select class="form-control mb-3" id="default_approval" name="default_approval" required="required">
                                    <option value="">Select your option</option>
                                    <option value="Yes" @if ($approval->default_approval == 'Yes') selected="selected" @endif>Yes</option>
                                    <option value="No" @if ($approval->default_approval == 'No') selected="selected" @endif>No</option>
                                </select>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="user_id">Work Order (W/O) ID </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="id_approval" name="id_approval" value="{{$approval->id_approval}}">
                                <input type="text" class="form-control" id="woid" name="woid" placeholder="Input WOID" required="required" @if ($approval->default_approval == 'Yes') readonly="readonly" @endif value="{{$approval->woid}}">
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="user_id">USER ID </label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="user_id" name="user_id" placeholder="Input user id as approval" required="required" value="{{$approval->user_id}}">
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 text-left">
                                <a href="{{ URL::previous() }}" class="btn btn-dark" id="back_button">
                                    <i class="back_button_i fas fa-arrow-left"></i> <span class="back-text">Back</span></a>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-lge" id="edit_data"><i
                                        class="edit_data_i fas fa-edit"></i>&nbsp;<span class="edit-text">Edit</span></button>
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-warning" id="cancel_button" data-loading-text="Clearing field..."><i
                                        class="cancel_button_i fas fa-undo-alt"></i>&nbsp;<span class="cancel-text">Reset</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    </header>
@stop

@section('js-section')
    <!-- add custom javascript here -->
    <script src="{{ URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript">
        popupConfirmationModal(".buttonModalLogout");
        logoutModalDismiss(".logout-modal-dismiss");
        logoutModalConfirm(".logout-modal-confirm", "GET", "/api-v1/logout");
        nProgressLoading();
        backButton('#back_button');
        cancelButton('#approvalSettingsForm','#cancel_button');
        editButton("#edit_data","POST","/api-v1/approval-settings/update-approval","#approvalSettingsForm");
        defaultApprovalSelect('#default_approval','#woid');
    </script>
    <!-- end custom javascript here -->
@stop
