@extends('layouts.app')
@section('css-section')
    @include('layouts.css-datatables');
@stop

@section('content')
    <header class="page-header">
        <h2><e class="fas fa-tags" aria-hidden="true"></e> @lang('label.label')</h2>
    </header>
    <div class="row">
        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    <font style="font-size: 20px;">Today : {{$now}}</font>
                </header>
                <div class="card-body card-body-modified">
                    <table class="table table-bordered label-datatables">
                        <thead>
                        <tr>
                            <th rowspan="2">NO</th>
                            <th rowspan="2">LINE</th>
                            <th rowspan="2">WO</th>
                            <th rowspan="2">MODEL SUFFIX</th>
                            <th rowspan="2">FLAG</th>
                            <th rowspan="2">PST</th>
                            <th rowspan="2">D-</th>
                            <th rowspan="2">SHIP NAME</th>
                            <th rowspan="2">TOTAL QTY</th>
                            <th rowspan="2">REMAIN QTY</th>
                            <th rowspan="2">COMPLETED QTY</th>
                            <th rowspan="2">ACTION</th>
                            <th colspan="2">SET</th>
                            <th colspan="2">LABEL</th>
                            <th rowspan="2">APPROVAL</th>
                            <th rowspan="2">REMARK</th>
                        </tr>
                        <tr>
                            <th>SET LABEL</th>
                            <th>SET LABEL DRAWING</th>
                            <th>BOX LABEL</th>
                            <th>BOX LABEL DRAWING</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </section>
        </div>
    </div>
    </header>
@stop

@section('js-section')
    @include("layouts.js-datatables")
    <!-- add custom javascript here -->
    <script src="{{ URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript">
        var calcDataTableHeight = function () {
            return $(window).height() * 45 / 100;
        };
        $(window).bind('resize', function () {
            $('.label-datatables').dataTable().css('width', '100%');
        });
        popupConfirmationModal(".buttonModalLogout");
        logoutModalDismiss(".logout-modal-dismiss");
        logoutModalConfirm(".logout-modal-confirm", "GET", "/api-v1/logout");
        nProgressLoading();
        $(function () {
            var table = $('.label-datatables').DataTable({
                processing: true,
                paging: true,
                "lengthMenu": [[5, 10, 25, 50, 100, 500, 1000 - 1], [5, 10, 25, 50, 100, 500, 1000, "All"]],
                "pageLength": 10,
                pagingType: "full_numbers",
                serverSide: true,
                "scrollY": calcDataTableHeight(),
                "scrollX": true,
                "scrollCollapse": true,
                sAjaxSource: "{{ route('label.list') }}",
                order: [[6, 'desc']],
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'NO',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {data: 'fa_pcsgid'},
                    {data: 'wo_name'},
                    {data: 'prodid'},
                    {data: 'new_mdl_flag'},
                    {
                        data: 'prdtn_strt_date', render: function (data, type, row) {
                            return dateFormat(data, "d mmmm yyyy h:MM:ss");
                        }
                    },
                    {data: 'days_before'},
                    {data: 'ship_to_customer_name'},
                    {data: 'tot_qty'},
                    {data: 'rmn_qty'},
                    {data: 'complt_qty'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'set_label', render: function (data, type, row) {
                            if (!data || data === "") {
                                return "N/A";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'set_label_drawing', render: function (data, type, row) {
                            if (!data || data === "") {
                                return "N/A"
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'box_label', render: function (data, type, row) {
                            if (!data || data === "") {
                                return "N/A";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'box_label_drawing', render: function (data, type, row) {
                            if (!data || data === "") {
                                return "N/A";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'default_approval', render: function(data, type, row) {
                            if (!data || data === "") {
                                return "N/A";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'woid', render: function(data, type, row) {
                            if (!data || data === "") {
                                return "N/A";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "oLanguage": {
                    "sProcessing": "Processing ....."
                },

                "fnPreDrawCallback": function () {
                    $('.dataTables_processing').attr('style', 'font-size: 16px; font-weight: bold; background: #000000;');
                },
                "fnInitComplete": function (oSettings, json) {
                },
                "fnDrawCallback": function () {

                },
            });
            var searchWaitInterval = null;
            $('.dataTables_filter input[type=search]').attr('placeholder', 'Looking for data....')
                .off()
                .on('input', function (e) {
                    var item = $(this);
                    var searchWait = 0;
                    if (!searchWaitInterval) searchWaitInterval = setInterval(function () {
                        if (searchWait >= 3) {
                            clearInterval(searchWaitInterval);
                            searchWaitInterval = null;
                            searchTerm = $(item).val();
                            $("#" + item.attr("aria-controls")).DataTable().search(searchTerm).draw();
                            searchWait = 0;
                        }
                        searchWait++;
                    }, 500);

                });
        });
    </script>
    <!-- end custom javascript here -->
@stop



