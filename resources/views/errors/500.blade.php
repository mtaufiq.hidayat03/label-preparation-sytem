@if(session()->has('user_id'))
    @include('errors.500-session')
@else
    @include('errors.500-without-session')
@endif
