@if(session()->has('user_id'))
    @include('errors.404-session')
@else
    @include('errors.404-without-session')
@endif
