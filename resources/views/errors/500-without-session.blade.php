<!doctype html>
<html class="fixed sidebar-light sidebar-left-collapsed">
<head>

    @include('layouts.meta')

    @include('layouts.css')

</head>
<body>
<!-- start: page -->
<section class="body-error error-inside">
    <div class="center-error">
        <div class="row">
            <div class="col-lg-8">
                <div class="main-error mb-3">
                    <h2 class="error-code text-dark text-center font-weight-semibold m-0">404 <i class="fas fa-tired"></i></h2>
                    <p class="error-explanation text-center">We're sorry, something went wrong.</p>
                </div>
            </div>
            <div class="col-lg-4">
                <img src="{{ URL::asset('img/error_500.png')}}" height="220" alt="Error 500"/>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->
@include('layouts.js')

</body>
</html>
