<?php


namespace App\Http\Controllers;

use App\Models\ApprovalSettings;

class ApprovalSettingController extends Controller
{
    public function index() {
        return view('approval-settings.index');
    }

    public function add() {
        return view('approval-settings.add');
    }

    public function edit($id) {
        $approval = ApprovalSettings::where('id_approval', $id)->first();
        return view('approval-settings.edit',['approval' => $approval]);
    }
}
