<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;


class LoginController
{
    public function index(Request $request) {
        if (Session::has('user_id')) {
            return redirect('label');
        } else {
            return view("login");
        }
    }
}
