<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ApprovalHistory;
use App\Models\ApprovalSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Route;
use Validator;
use Input;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class ApprovalHistoryApiController extends Controller
{
    public function getAllApprovalHistory(Request $request)
    {
        if ($request->ajax()) {
            $limit = ($request->get('iDisplayLength') ? $request->get('iDisplayLength') : 10);
            $offset = $request->get('iDisplayStart');
            $columnNo = ($request->get('iSortCol_0') ? $request->get('iSortCol_0') : '0');
            $sortBy = ($request->get('sSortDir_0') ? $request->get('sSortDir_0') : 'asc');
            if ($columnNo > 0) {
                $columnNo = $columnNo - 1;
            }
            $columns = ['ID_APPROVAL','USER_ID', 'STATUS_APPROVAL', 'DOCUMENT_APPROVAL', 'WOID', 'CREATED_AT', 'UPDATED_AT','ID'];
            $columnName = Str::lower($columns[($columnNo)]);
            $approvalHistory = ApprovalHistory::select($columns)
                ->where('DELETED_AT','=',null)
                ->orderBy($columnName, $sortBy);
            $count_total = $approvalHistory->count();
            if (!empty($request->get('sSearch'))) {
                foreach ($columns as $column) {
                    $approvalHistory->orWhere($column, 'LIKE', '%' . $request->get('sSearch') . '%');
                }
                $count_filter = $approvalHistory->count();
            } else {
                $count_filter = $count_total;
            }
            $data = $approvalHistory->get($columns)->skip($offset)->take($limit);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group">' .
                        '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' .
                        'Action</button>' .
                        '<div class="dropdown-menu">' .
                        '<a href="'.URL::to('/approval-settings/edit', $row['id_approval']).'" class="dropdown-item edit-data"><i class="edit_data_i fas fa-edit"></i> Edit Data</a>' .
                        '<div class="dropdown-divider"></div>' .
                        ' <a href="#modalDeleteApproval" class="dropdown-item buttonModalDeleteApproval" data-id="'.$row['id_approval'].'" data-default="'.$row['woid'].'"." data-default2="'.$row['user_id'].'"><i
                                        class="cancel_button_i fas fa-trash-alt"></i> Delete Data</a>' .
                        '</div></div>';
                    return $actionBtn;
                })
                ->with([
                    "columnNo" => $columnNo,
                    "columnName" => $columnName,
                    "sortBy" => $sortBy,
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->rawColumns(['action'])
                ->make(true);
        } else {
            return response()->json(["message" => "Forbidden Access"], 403);
        }
    }

    public function softDeleteApprovalHistory(Request $request) {

    }
}
