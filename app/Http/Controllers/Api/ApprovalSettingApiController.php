<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ApprovalSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Route;
use Validator;
use Input;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class ApprovalSettingApiController extends Controller
{
    public function getAllApprovalSettings(Request $request)
    {
        if ($request->ajax()) {
            $limit = ($request->get('iDisplayLength') ? $request->get('iDisplayLength') : 10);
            $offset = $request->get('iDisplayStart');
            $columnNo = ($request->get('iSortCol_0') ? $request->get('iSortCol_0') : '0');
            $sortBy = ($request->get('sSortDir_0') ? $request->get('sSortDir_0') : 'asc');
            if ($columnNo > 0) {
                $columnNo = $columnNo - 1;
            }
            $columns = ['WOID', 'USER_ID', 'DEFAULT_APPROVAL','CREATED_AT', 'UPDATED_AT', 'DELETED_AT', 'ID_APPROVAL'];
            $columnName = Str::lower($columns[($columnNo)]);
            $approvalSettings = ApprovalSettings::select($columns)
                ->where('DELETED_AT','=',null)
                ->orderBy($columnName, $sortBy);
            $count_total = $approvalSettings->count();
            if (!empty($request->get('sSearch'))) {
                foreach ($columns as $column) {
                    $approvalSettings->orWhere($column, 'LIKE', '%' . $request->get('sSearch') . '%');
                }
                $count_filter = $approvalSettings->count();
            } else {
                $count_filter = $count_total;
            }
            $data = $approvalSettings->get($columns)->skip($offset)->take($limit);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group">' .
                        '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' .
                        'Action</button>' .
                        '<div class="dropdown-menu">' .
                        '<a href="'.URL::to('/approval-settings/edit', $row['id_approval']).'" class="dropdown-item edit-data"><i class="edit_data_i fas fa-edit"></i> Edit Data</a>' .
                        '<div class="dropdown-divider"></div>' .
                        ' <a href="#modalDeleteApproval" class="dropdown-item buttonModalDeleteApproval" data-id="'.$row['id_approval'].'" data-default="'.$row['woid'].'"." data-default2="'.$row['user_id'].'"><i
                                        class="cancel_button_i fas fa-trash-alt"></i> Delete Data</a>' .
                        '</div></div>';
                    return $actionBtn;
                })
                ->with([
                    "columnNo" => $columnNo,
                    "columnName" => $columnName,
                    "sortBy" => $sortBy,
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->rawColumns(['action'])
                ->make(true);
        } else {
            return response()->json(["message" => "Forbidden Access"], 403);
        }
    }

    public function saveApprovalSettings(Request $request)
    {
        if ($request->ajax()) {
            $rules = array (
              'woid' => 'required',
              'user_id' => 'required',
              'default_approval' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['message' => 'Please, re-checking all fields in this form'], 500);
            } else {
                if (ApprovalSettings::where([['woid', $request->woid],['user_id',$request->user_id]])->exists()) {
                    return response()->json(['message' => 'User: '.$request->user_id.' has been registered as approval for woid: '.$request->woid], 500);
                } else {
                    try {
                        $approvalSetting = new ApprovalSettings;
                        $approvalSetting->woid = $request->woid;
                        $approvalSetting->user_id = $request->user_id;
                        $approvalSetting->default_approval = $request->default_approval;
                        $approvalSetting->created_at = Carbon::now();
                        $approvalSetting->save();
                        return response()->json(["message" => "User: ".$request->user_id." is succesfully registered as approval for woid: ".$request->woid,
                            "payload" => URL::to('/approval-settings')], 200);
                    } catch (\Exception $e) {
                        return response()->json(["message" => "Error: ". $e->getMessage()], 400);
                    }
                }
            }
        } else {
            return response()->json(["message" => "Forbidden access"], 403);
        }
    }

    public function updateApprovalSettings(Request $request)
    {
        if ($request->ajax()) {
            $rules = array (
                'id_approval' => 'required',
                'woid' => 'required',
                'user_id' => 'required',
                'default_approval' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['message' => 'Please, re-checking all fields in this form'], 500);
            } else {
                if (ApprovalSettings::where([['woid', $request->woid],['user_id',$request->user_id]])->exists()) {
                    return response()->json(['message' => 'User: '.$request->user_id.' has been registered as approval for woid: '.$request->woid], 500);
                } else {
                    try {
                        $approvalSetting = ApprovalSettings::where('id_approval', $request->id_approval)->first();
                        $approvalSetting->woid = $request->woid;
                        $approvalSetting->user_id = $request->user_id;
                        $approvalSetting->default_approval = $request->default_approval;
                        $approvalSetting->updated_at = Carbon::now();
                        $approvalSetting->save();
                        return response()->json(["message" => "User: ".$request->user_id." is succesfully edited as approval for woid: ".$request->woid,
                            "payload" => URL::to('/approval-settings')], 200);
                    } catch (\Exception $e) {
                        return response()->json(["message" => "Error: ". $e->getMessage()], 400);
                    }
                }
            }
        } else {
            return response()->json(["message" => "Forbidden access"], 403);
        }
    }

    public function softDeleteApprovalSettings(Request $request)
    {
        if ($request->ajax()) {
            $approvalSettings = ApprovalSettings::find($request->deleteId);
            if ($approvalSettings->exists()) {
                try {
                    $approvalSettings->deleted_at = Carbon::now();
                    $approvalSettings->save();
                    return response()->json(["message" => "Approval setting is deleted succefully"], 200);
                } catch (\Exception $e) {
                    return response()->json(["message" => "Error: " . $e->getMessage()], 400);
                }
            } else {
                return response()->json(["message" => "Data not found"], 404);
            }
        } else {
            return response()->json(["message" => "Forbidden access"], 403);
        }
    }
}
