<?php

namespace App\Http\Controllers\Api;

use App\Models\ApprovalSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use \App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LabelApiController extends Controller
{

    public function getAllLabel(Request $request)
    {
        if ($request->ajax()) {
            $limit = ($request->get('iDisplayLength') ? $request->get('iDisplayLength') : 10);
            $offset = $request->get('iDisplayStart');
            $columnNo = ($request->get('iSortCol_0') ? $request->get('iSortCol_0') : '0');
            $sortBy = ($request->get('sSortDir_0') ? $request->get('sSortDir_0') : 'asc');
            if ($columnNo > 0) {
                $columnNo = $columnNo - 1;
            }

            $approvals = ApprovalSettings::where('DEFAULT_APPROVAL','Yes')->get(['user_id']);
            $approve = "";
            foreach ($approvals as $approval) {
                $approve .= $approval->user_id .",";
            }
            $approveHelper = substr($approve,0,-1);

            $columns = ['WORKORDER_GMES.FA_PCSGID','WORKORDER_GMES.WO_NAME','WORKORDER_GMES.PRODID',
                'TB_POM_DILY_PRDTN_PLN_GMES.NEW_MDL_FLAG', 'TB_POM_DILY_PRDTN_PLN_GMES.PRDTN_STRT_DATE',
                DB::raw('(ROUND(SYSDATE-TB_POM_DILY_PRDTN_PLN_GMES.PRDTN_STRT_DATE)) AS DAYS_BEFORE'),
                'TB_POM_DILY_PRDTN_PLN_GMES.SHIP_TO_CUSTOMER_NAME', 'TB_POM_DILY_PRDTN_PLN_GMES.TOT_QTY',
                'TB_POM_DILY_PRDTN_PLN_GMES.RMN_QTY','TB_POM_DILY_PRDTN_PLN_GMES.COMPLT_QTY',
                'LPS_UPLOAD_LABEL.SET_LABEL', 'LPS_UPLOAD_LABEL.SET_LABEL_DRAWING',
                'LPS_UPLOAD_LABEL.BOX_LABEL', 'LPS_UPLOAD_LABEL.BOX_LABEL_DRAWING',
                DB::raw("'" . $approveHelper . "' AS DEFAULT_APPROVAL"),
                'WORKORDER_GMES.WO_NAME'
            ];
            if (strpos($columns[($columnNo)], ' AS ') !== FALSE) {
                $columnName = Str::lower(substr($columns[($columnNo)], strpos($columns[($columnNo)], ' AS ') + strlen(' AS ')));
            } else {
                $columnName = Str::lower($columns[($columnNo)]);
            }
            /*
            $workOrders =DB::table('WORKORDER_GMES')
                ->join('TB_POM_DILY_PRDTN_PLN_GMES','WORKORDER_GMES.WOID', '=', 'TB_POM_DILY_PRDTN_PLN_GMES.WOID')
                ->join('LPS_UPLOAD_LABEL','LPS_UPLOAD_LABEL.WOID','=','WORKORDER_GMES.WOID', 'full outer')
                ->select($columns)
                ->where('TB_POM_DILY_PRDTN_PLN_GMES.PRDTN_STRT_DATE',
                    \DB::raw('(SELECT MAX(PRDTN_STRT_DATE) FROM TB_POM_DILY_PRDTN_PLN_GMES WHERE WOID=WORKORDER_GMES.WOID)'))
                ->where([
                    [\DB::raw('(SELECT ROUND(SYSDATE-MAX(PRDTN_STRT_DATE)) FROM TB_POM_DILY_PRDTN_PLN_GMES WHERE WOID=WORKORDER_GMES.WOID)'), '>', '-8'],
                    [\DB::raw('(SELECT ROUND(SYSDATE-MAX(PRDTN_STRT_DATE)) FROM TB_POM_DILY_PRDTN_PLN_GMES WHERE WOID=WORKORDER_GMES.WOID)'), '<', '0'],
                ])
                ->orderBy($columnName, $sortBy);
            */
            //FIXING FULL OUTER JOIN
            $workOrders =DB::table('WORKORDER_GMES')
                    ->join('TB_POM_DILY_PRDTN_PLN_GMES','WORKORDER_GMES.WOID', '=', 'TB_POM_DILY_PRDTN_PLN_GMES.WOID')
                    ->join('LPS_UPLOAD_LABEL','LPS_UPLOAD_LABEL.WOID','=','WORKORDER_GMES.WOID', 'full outer')
                    //->join('LPS_APPROVAL_SETTINGS', 'LPS_APPROVAL_SETTINGS.DEFAULT_APPROVAL','=', \DB::raw("'Yes'"), 'full outer')
                    ->select($columns)
                    ->where('TB_POM_DILY_PRDTN_PLN_GMES.PRDTN_STRT_DATE',
                        \DB::raw('(SELECT MAX(PRDTN_STRT_DATE) FROM TB_POM_DILY_PRDTN_PLN_GMES WHERE WOID=WORKORDER_GMES.WOID)'))
                    ->where([
                        [\DB::raw('(SELECT ROUND(SYSDATE-MAX(PRDTN_STRT_DATE)) FROM TB_POM_DILY_PRDTN_PLN_GMES WHERE WOID=WORKORDER_GMES.WOID)'), '>', '-10'],
                        [\DB::raw('(SELECT ROUND(SYSDATE-MAX(PRDTN_STRT_DATE)) FROM TB_POM_DILY_PRDTN_PLN_GMES WHERE WOID=WORKORDER_GMES.WOID)'), '<', '60'],
                    ])
                    ->orderBy($columnName, $sortBy);
            //enable return eloquent query builder into sql
            //$toSql = $workOrders->toSql();
            $count_total = $workOrders->count();
            if (!empty($request->get('sSearch'))) {
                foreach ($columns as $column) {
                    $workOrders->orWhere($column, 'LIKE', '%' . $request->get('sSearch') . '%');
                }
                $count_filter = $workOrders->count();
            } else {
                $count_filter = $count_total;
            }
            $data = $workOrders->get($columns)->skip($offset)->take($limit);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group">' .
                        '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' .
                        'Action</button>' .
                        '<div class="dropdown-menu">'.
                        '<a href="'.URL::to('/label/upload', $row->wo_name).'" class="dropdown-item upload-data"><i class="upload_data_i fas fa-upload"></i> Upload Label</a>' .
                        '<div class="dropdown-divider"></div>' .
                        '<a href="#modalDeleteLabel" class="dropdown-item buttonModalDeleteLabel" data-id="'.$row->wo_name.'" data-default="'.$row->wo_name.'"><i
                                        class="cancel_button_i fas fa-trash-alt"></i> Delete Label</a>' .
                        '<div class="dropdown-divider"></div>' .
                        ' <a href="'.URL::to('/label/approval', $row->wo_name).'" class="dropdown-item approval-data"><i class="approval_data_i fas fa-check-circle"></i> Approval</a>'.
                        '</div></div>';
                    return $actionBtn;
                })
                ->with([
                    //"toSql" => $toSql,
                    "columnNo" => $columnNo,
                    "columnName" => $columnName,
                    "sortBy" => $sortBy,
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->rawColumns(['action'])
                ->make(true);
        }
    }

}
