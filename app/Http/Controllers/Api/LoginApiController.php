<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Session;
use Validator;
use Input;
use Illuminate\Support\Facades\URL;
use Route;

class LoginApiController extends Controller
{

    public function doLoginAccount(Request $request)
    {
        $username = $request->username;
        $password = sha1($request->password);
        $rules = array(
            'username' => 'required',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['message' => "Username and Password must not be empty"], 500);
        } else {
            /* RAW SQL
            $user = DB::select(DB::raw("SELECT * FROM LPS_USERS where password = :password
                and (user_id = :username or employee_no = :username or email = :username"),
                array('password' => $password, 'username' => $username));
            */
            $user = User::select('user_id', 'employee_no', 'email')
                ->where(function ($query) use ($username) {
                    $query->where('user_id', '=', $username)
                        ->orWhere('employee_no', '=', $username)
                        ->orWhere('email', '=', $username);
                })->where('password', '=', $password)->first();
            if ($user) {
                $result = json_decode($user, true);
                $request->session()->put('user_id', $result['user_id']);
                $request->session()->put('email', $result['email']);
                return response()->json(['message' => "Login successfully.&nbsp;<b> Welcome, ".$result['user_id']." </b>. You'll redirect to home page.",
                    "payload" => URL::to('/label')], 200);
            } else {
                return response()->json(['message' => 'Unauthorized. Wrong combination between username and password'], 401);
            }
        }
    }

    public function forgetPassword(Request $request)
    {

    }

    public function doLogoutAccount(Request $request)
    {
        $request->session()->forget('user_id');
        $request->session()->forget('email');
        return response()->json(['message' => "Logout successfully. You'll redirect to login page.",
            "payload" => URL::to('/')], 200);
    }

}
