<?php


namespace App\Http\Controllers;

use App\Models\EmailNotification;

class EmailNotificationSettingController extends Controller
{
    public function index() {
        return view('email-notification.index');
    }

    public function add() {
        return view('email-notification.add');
    }

    public function edit($id) {
        $email = EmailNotification::find($id);
        return view('email-notification.edit',['email' => $email]);
    }

}
