/* Add here all your JS customizations */
function popupConfirmationModal(className) {
    $(className).magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom',
        modal: true
    });
}

function nProgressLoading() {
    <!-- NProgress Config -->
    NProgress.configure({showSpinner: true});
    // Show the progress bar
    NProgress.start();
    // Trigger finish when page fully loaded
    $(window).on('load', function () {
        NProgress.done();
    });
    // Trigger bar when exiting the page
    $(window).on('unload', function () {
        NProgress.start();
    });
    <!-- End of NProgress Config -->
}

function submitDataAjaxLogin(idNameButton, typeAjax, urlAjax, idForm) {
    $.ajax({
        type: typeAjax,
        url: urlAjax,
        serverSide: true,
        processing: true,
        cache: false,
        data: $(idForm).serialize(),
        beforeSend: function () {
            $('.sign-in').removeClass('fa-user mr-1');
            $('.sign-in').addClass('fa-sync fa-spin');
            $('.sign-in-text').text("Loading...");
            $(idNameButton).attr("disabled", true);
        },
        error: function (data, status, error) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
            })

            Toast.fire({
                icon: 'error',
                title: data.responseJSON.message,
                showClass: {
                    popup: 'animate__animated animate__bounce'
                },
                hideClass: {
                    popup: 'animate__animated animate__slideOutUp'
                }
            })
        },
        complete: function () {
            $('.sign-in').removeClass('fa-sync fa-spin');
            $('.sign-in').addClass('fa-user mr-1');
            $('.sign-in-text').text("Sign In");
            $(idNameButton).attr("disabled", false);
        },
        success: function (data, status, xhr) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
            });

            Toast.fire({
                icon: 'success',
                title: xhr.responseJSON.message,
                showClass: {
                    popup: 'animate__animated animate__bounce'
                },
                hideClass: {
                    popup: 'animate__animated animate__slideOutUp'
                }
            });
            setTimeout(
                function () {
                    if (xhr.responseJSON.payload) {
                        window.location = xhr.responseJSON.payload
                    }
                },
                2000);
        },
    });
}

function postDataAjaxLoginByIdButton(idNameButton, typeAjax, urlAjax, idForm) {
    $(idNameButton).click(function (e) {
        e.preventDefault();
        submitDataAjaxLogin(idNameButton, typeAjax, urlAjax, idForm);
    });
}

function logoutModalDismiss(className) {
    $(document).on('click', className, function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
}

function logoutModalConfirm(className, typeAjax, urlAjax) {
    $(document).on('click', className, function (e) {
        e.preventDefault();
        $.magnificPopup.close();
        $.ajax({
            type: typeAjax,
            url: urlAjax,
            serverSide: true,
            bProcessing: true,
            cache: false,
            error: function (data, status, error) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                })

                Toast.fire({
                    icon: 'error',
                    title: data.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                })
            },
            success: function (data, status, xhr) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                });

                Toast.fire({
                    icon: 'success',
                    title: xhr.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                });
                setTimeout(
                    function () {
                        if (xhr.responseJSON.payload) {
                            window.location = xhr.responseJSON.payload
                        }
                    },
                    2000);
            },
        });
    });
}

function backButton(backButtonId) {
    $(backButtonId).on('click', function () {
        $('.back_button_i').removeClass('fa-arrow-left');
        $('.back_button_i').addClass('fa-spinner fa-pulse');
        $('.back-text').text("Return page...");
    })
}

function cancelButton(formId, cancelButtonId) {
    $(cancelButtonId).on('click', function () {
        $('.cancel_button_i').removeClass('fa-trash-alt');
        $('.cancel_button_i').addClass('fa-spinner fa-pulse');
        $('.cancel-text').text("Clearing form...");
        $(formId).get(0).reset(setInterval(function () {
            $('.cancel_button_i').removeClass('fa-spinner fa-pulse');
            $('.cancel_button_i').addClass('fa-trash-alt');
            $('.cancel-text').text("Reset");
        }, 2000));

    })
}

function saveButton(idNameButton, typeAjax, urlAjax, idForm) {
    $(document).on('click', idNameButton, function (e) {
        e.preventDefault();
        $.magnificPopup.close();
        $.ajax({
            type: typeAjax,
            url: urlAjax,
            serverSide: true,
            bProcessing: true,
            cache: false,
            data: $(idForm).serialize(),
            beforeSend: function () {
                $('.save_data_i').removeClass('fa-save');
                $('.save_data_i').addClass('fa-spinner fa-pulse');
                $('.save-text').text("Saving data...");
            },
            error: function (data, status, error) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                });
                Toast.fire({
                    icon: 'error',
                    title: data.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                });
                $('.save_data_i').removeClass('fa-spinner fa-pulse');
                $('.save_data_i').addClass('fa-save');
                $('.save-text').text("Save");
            },
            success: function (data, status, xhr) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                });

                Toast.fire({
                    icon: 'success',
                    title: xhr.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                });
                setTimeout(
                    function () {
                        if (xhr.responseJSON.payload) {
                            window.location = xhr.responseJSON.payload
                        }
                    },
                    2000);
            },
            complete: function () {
                $('.save_data_i').removeClass('fa-spinner fa-pulse');
                $('.save_data_i').addClass('fa-save');
                $('.save-text').text("Save");
            }
        });
    });
}

function editButton(idNameButton, typeAjax, urlAjax, idForm) {
    $(document).on('click', idNameButton, function (e) {
        e.preventDefault();
        $.magnificPopup.close();
        $.ajax({
            type: typeAjax,
            url: urlAjax,
            serverSide: true,
            bProcessing: true,
            cache: false,
            data: $(idForm).serialize(),
            beforeSend: function () {
                $('.edit_data_i').removeClass('fa-edit');
                $('.edit_data_i').addClass('fa-spinner fa-pulse');
                $('.edit-text').text("Updating data...");
            },
            error: function (data, status, error) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                });
                Toast.fire({
                    icon: 'error',
                    title: data.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                });
                $('.edit_data_i').removeClass('fa-spinner fa-pulse');
                $('.edit_data_i').addClass('fa-edit');
                $('.edit-text').text("Edit");
            },
            success: function (data, status, xhr) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                });

                Toast.fire({
                    icon: 'success',
                    title: xhr.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                });
                setTimeout(
                    function () {
                        if (xhr.responseJSON.payload) {
                            window.location = xhr.responseJSON.payload
                        }
                    },
                    2000);
            },
            complete: function () {
                $('.edit_data_i').removeClass('fa-spinner fa-pulse');
                $('.edit_data_i ').addClass('fa-edit');
                $('.edit-text').text("Edit");
            }
        });
    });
}

function deleteButton(idNameButton, typeAjax, urlAjax, idForm) {
    $(document).on('click', idNameButton, function (e) {
        e.preventDefault();
        $.magnificPopup.close();
        $.ajax({
            type: typeAjax,
            url: urlAjax,
            serverSide: true,
            bProcessing: true,
            cache: false,
            data: $(idForm).serialize(),
            error: function (data, status, error) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                })

                Toast.fire({
                    icon: 'error',
                    title: data.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                })
            },
            success: function (data, status, xhr) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                });

                Toast.fire({
                    icon: 'success',
                    title: xhr.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                });
                setTimeout(
                    function () {
                        if (xhr.responseJSON.payload) {
                            window.location = xhr.responseJSON.payload
                        }
                    },
                    2000);
            },
        });
    });
}


function deleteModalDismiss(className) {
    $(document).on('click', className, function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
}

function deleteModalConfirm(classId, typeAjax, urlAjax, tableClassName, idForm) {
    $(classId).on("click", function (e) {
        e.preventDefault();
        $.magnificPopup.close();
        $.ajax({
            type: typeAjax,
            url: urlAjax,
            serverSide: true,
            bProcessing: true,
            cache: false,
            data: $(idForm).serialize(),
            error: function (data, status, error) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                })

                Toast.fire({
                    icon: 'error',
                    title: data.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                })
            },
            success: function (data, status, xhr) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                });

                Toast.fire({
                    icon: 'success',
                    title: xhr.responseJSON.message,
                    showClass: {
                        popup: 'animate__animated animate__lightSpeedInRight'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__lightSpeedOutRight'
                    }
                });
                $(tableClassName).DataTable().ajax.reload(null, false);
                setTimeout(
                    function () {
                        if (xhr.responseJSON.payload) {
                            window.location = xhr.responseJSON.payload
                        }
                    },
                    2000);
                return false;
            },
        });
        $(this).unbind('click', arguments.callee);
    });
}

function getDataAttributes(el) {
    var data = {};
    [].forEach.call(el.attributes, function (attr) {
        if (/^data-/.test(attr.name)) {
            var camelCaseName = attr.name.substr(5).replace(/-(.)/g, function ($0, $1) {
                return $1.toUpperCase();
            });
            data[camelCaseName] = attr.value;
        }
    });
    return data;
}


function popupDeleteConfirmationModal(className, appendDescription, nameField, nameField2) {
    $(className).magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom',
        modal: true,
        callbacks: {
            open: function () {
                var magnificPopup = $.magnificPopup.instance,
                    cur = magnificPopup.st.el;
                if (nameField && nameField !== "") {
                    $(appendDescription).append(nameField + ": " + cur.attr('data-default'));
                }
                if (nameField2 && nameField2 !== "") {
                    $(appendDescription).append("<br/>");
                    $(appendDescription).append(nameField2 + ": " + cur.attr('data-default2'));
                }
                $('.deleteId').val(cur.attr('data-id'));
            },
            close: function () {
                $(appendDescription).html('');
            }
        }
    });
}

var dateFormat = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};

function defaultApprovalSelect(selectId, textFieldDisabled) {
    $(selectId).on('change', function() {
        if (this.value === "Yes") {
            $(textFieldDisabled).attr("readonly", true);
            $(textFieldDisabled).val("default");
        } else {
            $(textFieldDisabled).attr("readonly", false);
            $(textFieldDisabled).val("");
        }
    });
}
